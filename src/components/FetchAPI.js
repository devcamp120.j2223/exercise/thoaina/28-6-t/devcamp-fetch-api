import { Component } from "react";

class FetchAPI extends Component {
    fetchAPI = async (url, body) => {
        let res = await fetch(url, body);
        let data = await res.json();
        return data;
    }

    getAllAPI = () => {
        console.log("Get All API");
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts")
            .then((data) => console.log(data))
    }

    getAPIById = () => {
        console.log("Get API by ID");
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts/1")
            .then((data) => console.log(data))
    }

    createAPI = () => {
        let body = {
            method: 'POST',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        console.log("Create API");
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts", body)
            .then((data) => console.log(data))
    }

    updateAPI = () => {
        console.log("Update API");
        let body = {
            method: 'PUT',
            body: JSON.stringify({
                id: 1,
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => console.log(data))
    }

    deleteAPI = () => {
        console.log("Delete API");
        let body = {
            method: "DELETE"
        }
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => console.log(data));
    }

    render() {
        return (
            <div className="row mt-5">
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.getAllAPI}>Get All API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.getAPIById}>Get API by ID</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.createAPI}>Create API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.updateAPI}>Update API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.deleteAPI}>Delete API</button>
                </div>
            </div>
        )
    }
}

export default FetchAPI;
