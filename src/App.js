import "bootstrap/dist/css/bootstrap.min.css";
import FetchAPI from "./components/FetchAPI";

function App() {
  return (
    <div className="container">
      <FetchAPI />
    </div>
  );
}

export default App;
